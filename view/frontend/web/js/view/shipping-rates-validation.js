/**
 * Copyright © 2018 Madev. All rights reserved.
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-rates-validation-rules',
        '../model/shipping-rates-validator',
        '../model/shipping-rates-validation-rules'
    ],
    function (
        Component,
        defaultShippingRatesValidator,
        defaultShippingRatesValidationRules,
        otherprovidersShippingRatesValidator,
        otherprovidersShippingRatesValidationRules
    ) {
        "use strict";
        defaultShippingRatesValidator.registerValidator('otherproviders', otherprovidersShippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('otherproviders', otherprovidersShippingRatesValidationRules);
        return Component;
    }
);
